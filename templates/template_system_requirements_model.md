# PlantUML code in Markdown file

```puml
@startuml
partition Requirement
(*) --> ===R===
note on link:
/'[requirement id=REQ0010 story=US0005,US0007 quality=QR0001 test=TC0004,TC0006]'/
end partition

partition Design
===R=== --> ===D===
end partition

partition Development
partition "Sub Design Part"
===D=== --> ===SD===
end partition

partition "Sub Development Part"
===SD=== --> ===SP===
end partition

partition "Sub Testing Part"
===SP=== --> ===ST===
end partition
end partition

partition Verification
===ST=== --> ===SV===
end partition

partition Preview
===SV=== --> ===SPR===
end partition

partition Operation
partition "Production Launch"
===SPR=== --> ===SPL===
end partition
===SPL=== --> (*)
end partition
@enduml
```

## Another PlantUML diagram

```puml
@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response
Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response

note right of Alice
/'[requirement id=REQ0012 quality=QR0005 test=TC0005]'/
end note
@enduml
```

## DOT language embedded within markdown file

### Context diagram

```puml
/'[requirement id=REQ0002 test=TC0002]'/
@startdot
digraph contextDiagram {
    node[shape="record"]
    subgraph externalEntities {
        entity[label="Customer"];
        entity2[label="Manager"];
    }
    subgraph cluster_System {
        label="System environment";
        process1[label="System" shape="ellipse"];
        graph[style=dotted]
    }
    entity->process1[label="Request for services"];
    process1->entity2[label="Reports"];
}
@enddot
```
