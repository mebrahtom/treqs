# T-Reqs: Tool Support for Managing Requirements in Large-Scale Agile System Development

Requirements engineering is crucial to support agile development of large systems with long lifetimes.
Agile cross-functional teams must be aware of requirements at system level and able to efficiently propose updates to those requirements.

- *Objective:* T-Reqs' objective is to offer lightweight tooling to __manage requirements in large-scale agile system development__.
- *Philosophy:* __Bring requirements close to teams.__ T-Reqs aims to put requirements into the hands of teams and allows managing them together with changes of code and test.

Where many tools require an organization to adjust their process, T-Reqs aims to integrate into a particular organization's large-scale agile way of working.  
It supports proven solutions for requirements challenges in large-scale agile, for example updating system requirements based on using gerrit to review changes proposed by agile teams.

T-Reqs is currently in industrial use and based on its success, we are exploring whether providing its key concepts and infrastructure as open source is valuable.
Regardless, we hope our experience helps to trigger a dialogue on how the changing landscape of requirements engineering in relation to agile system and software development affects needs for tooling support.

## About this project

T-Reqs is a living tool. Based on scripts, it integrates git and related tools into a powerful solution to support agile teams at scale.
This project uses GitLab as a repository manager and code review tool and git as a version control.
Our scripts and setup may be reusable, e.g. through forking our project and using it to manage requirements.
But generally, we assume that an organization may use this project as a template for their own setup.

### Requirements

  Python 3.x

## Getting started

### Installation

Clone this repo, using the following command:

    git clone https://gitlab.com/mebrahtom/treqs.git

Then install the distribution in your system by running the following command (by changing directory to the "treqs" root directory-where the setup.py file exists)

    pip install -e .

To start working with treqs, create a new repository for your own project or clone an existing one.

After cloning your newly created or existing repository, create a **dev** branch on your repository. Then, run the following command from the root of your repository.

    treqsconfig

After running the **treqsconfig** command, you will be prompted to enter the following details:
The parameters in brackets are default values and if you want to keep the default values, just skip the current step by hitting the ENTER key!

- **The URL of your repository management service(https://gitlab.com)**: This refers to the base URL of your repository manager. The default is https://gitlab.com.
- **GitLab group name**: The name of the group where your repositories are placed on GitLab. This can be a gitlab/github username as well if you are working on your project alone.
- **Parent directory of T-reqs artifacts(```treqs```)**: This is the directory within your repository that houses system requirements, tests and other artifacts.
- **File name pattern for system requirements(```SR_.*?\\.md```)**: The filename pattern for system requirements (default: ```SR_.*?\\.md```)
- **ID prefix for system requirements(```REQ```)**:The initial characters for system requirement IDs (default:```REQ```)
- **File name pattern for quality requirements(```QR_.*?\\.md```)**: The file name pattern for quality requirements (default:```QR_.*?\\.md```)
- **ID prefix for quality requirements(REQ)**:The initial characters for quality requirement IDs (default:```QR```)
- **File name pattern for user stories(```US_.*?\\.md```)**: The filename pattern for user stories (default: ```US_.*?\\.md```)
- **ID prefix for user stories(```US```)**:The initial characters for user story IDs (default:```US```)
- **File name pattern for test cases(```TC_.*?(\\.py|\\.md)$```)**: The filename pattern for test cases (default: ```TC_.*?(\\.py|\\.md)$```)
- **ID prefix for test cases(```TC```)**:The initial characters for testcase IDs (default:```TC```)
- **File name pattern for acceptance tests(```AT_.*?\\.feature```)**: The filename pattern for acceptance tests (default: ```AT_.*?\\.feature```)
- **ID prefix for acceptance tests(```AT```)**:The initial characters for acceptance test IDs (default:```AT```)
The above details will be saved in configuration file **.config.json** under the parent directory of your artifacts (by default under the `treqs` directory). You can modify the values of the parameters in the **.config.json** file.

Before writing artifacts such as system requirements and quality requirements by following the corresponding templates provided with the treqs tool, you need to generate IDs for the artifact you want to write.

### Generating IDs

Before you generate IDs, you have to create the tables that store those IDs. In this version of T-reqs, Amazon Relational Database Service(RDS) is used to store the IDs of the artifacts. A sample database configuration on RDS looks like the following:

    HOST_NAME ="treqsdb.cwel9elimbjd.eu-central-1.rds.amazonaws.com"
    PORT_NUM = 3306
    USER_NAME = "admin"
    PASSWORD = "treqs123"
    DB_NAME = "treqsdb"
Once you setup your database configuration in the `dbConfig.py` file under the `treqs` python package in the T-reqs tool, the database and the tables will be created when you run the `generateid` command for the first time.

The tables to be created have the following structure:

```sql
CREATE TABLE IF NOT EXISTS sysreqid_tbl (
id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
sysreqid VARCHAR (20) UNIQUE NOT NULL,
user_email VARCHAR (60),
date_generated date
)
CREATE TABLE IF NOT EXISTS qualityreqid_tbl (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    qualityreqid VARCHAR (20) UNIQUE NOT NULL,
    user_email VARCHAR (60),
    date_generated date
)
CREATE TABLE IF NOT EXISTS testid_tbl (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    testid VARCHAR (20) UNIQUE NOT NULL,
    user_email VARCHAR (60),
    date_generated date
)
CREATE TABLE IF NOT EXISTS userstoryid_tbl (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    userstoryid VARCHAR (20) UNIQUE NOT NULL,
    user_email VARCHAR (60),
    date_generated date
)
CREATE TABLE IF NOT EXISTS acceptancetestid_tbl (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    acceptancetestid VARCHAR (20) UNIQUE NOT NULL,
    user_email VARCHAR (60),
    date_generated date
)
```

Once the tables are created, you can start generating IDs.
To generate ID/s, use the following command:

 `generateid -a name_of_artifact -n number_of_IDs`

 Usage:
      generateid -a name_of_artifact -n number_of_IDs

    Options (that can follow -a):
      -us for generating userstory IDs
      -sr for generating system requirement IDs
      -tc for for generating testcase IDs
      -at for generating acceptance test IDs
      -qr for generating quality requirement IDs
Example, the following command generates two IDs for user stories:

`generateid -a us -n 2`

If you want to generate one ID, run the command without the -n option.
Example, the following command generates one system requirement ID:

`generateid -a sr`

### Running scripts on example requirements

`treqs`
The treqs command produces a markdown log file (Summary_\*) in the logs folder (scripts/logs).
This log file contains a report of duplicate IDs, missing traces and missing items.

### Generating diagram previews for model-based system requirements

In T-reqs, model-based system requirements should be written in PlantUML and/or the DOT language. PlantUML (http://plantuml.com/) is a tool for writing UML diagrams in a textual form. The DOT language (http://www.graphviz.org/doc/info/lang.html) is a graph description language which is used to write graphs of different forms.
The PlantUML/DOT code should be written in markdown files using fenced-code blocks in the following form:

````
Markdown text
```puml
PlantUML/DOT code
```
More markdown text
````

After writing your model-based system requirements by following the templates, you have to execute the `generatepreview` command on your terminal as following:

`generatepreview markdown_file_path`

The `generatepreview` command generates trace-links and URLs to the corresponding diagrams in the same file.

With the help of `git pre-commit hooks`, the `generatepreview` command can be executed automatically.
All you need to do is committing your changes to the markdown file that contains model-based system requirements.

For this to work, you have to include the `generatepreview` command to the `.git/hooks/pre-commit` file.

## Working with multiple repositories

**Git submodules** can be used to manage system requirements and other related artifacts that are shared between multiple repositories.

To achieve this:

- Create a separate repository on Gitlab that houses the shared artifacts.
- Then clone the repository of shared artifacts as a git submodule of your own repository under the parent directory of your T-reqs artifacts (under treqs by default)
- It doesn't matter what directory name or structure you use as long as all your treqs artifacts are under the parent directory you specified during configuration. The default is **treqs**.
- To push shared treqs artifacts, first commit them by changing directory to the shared repository and push them with the following command:
  
`git push -u origin [branchname]`

- To pull shared artifacts into your own repository, run the following command from the root of your repository you want to pull into:
  
`git submodule update --remote --merge` or

`git submodule update --remote --rebase`

Another tool that can be used to manage the shared treqs artifacts is **git-subrepo** tool (https://github.com/ingydotnet/git-subrepo).

- Install the tool following the instructions in the documentation.
- Create a new repository and place the shared treqs artifacts in this repository.
- Then add the repository as gitsubrepo to your own primary repository so that you can work with the shared requirements as well.
- To push and pull shared artifacts using the git-subrepo tool use the following commands respectively:

    `git subrepo push name_of_repository`

    `git subrepo pull name_of_repository`

In order to automate the execution of the `generatepreview` command while working with shared model-based system requirements, you have to include the `generatepreview` command to the `./git/modules/submodule_path/hooks/pre-commit` file.

To work with artifacts that are not shared with other repositories, you don't need to place them in another repository.
You can use the regular git workflow.

## Change history of system requirements

The **git blame** command can be used to keep track of line-based change history of system requirements and other related artifacts that are written in files.
The following command will show you the change history of the lines in the provided file.

    git blame file_name

If you want a graphical-user-interface based display of the change history, use the following command:

    git gui blame file_name

### Running test cases for the treqs package

    ./run_tests.sh

## Project structure

- __templates__ This folder contains templates that indicate the syntax for requirement and test ids that our scripts rely on.
Adjust these requirements to fit your context.
- __treqs__ This folder contains scripts, e.g. to check if any user stories on github are not linked to from requirements, or if all requirements are covered by tests.

T-Reqs combines these things together and encourages each part to evolve as you develop software.
I.e. the scripts and templates should be evolved and adjusted to your organization's needs by the people that use them: The agile teams.

## Minimal setup

The minimal setup (as provided by the folders in this project) is as follows.

## Minimal setup* An .md file containing requirements simulates system requirements. For instance, the following text would represent a single requirement with id REQ1 that has traces to user stories US1 and US2:

```
Some supplementary text
[requirement id=REQ0001 story=US0001,US0002]
Requirement text
More supplementary text
```

* A test case file with test cases. Test cases can be markdown files (for manual test routines) or source files (here: python) (for automated test scripts). An example for a manual test case and a python test case are depicted below. In both cases, the [testcase] tag contains a test case id as well as tracing information to user stories and system requirements.

```
[testcase id=TestcaseID story=StoryID requirement=ReqID]
Purpose: Purpose of the test case without linebreak.

## Setup
Describe any steps that must be done before performing the test.

## Scenario / Steps

## Expected outcome

## Tear down
Describe what to do after the test
## Test result
Protocol of the result of executing this test, latest on top.
```

```
#==================================
# [testcase id=TC_python_test1 story=US4 requirement=REQ3]
#
# <Purpose of the test case>
#==================================
testcase TC1__test_case_name() {
...
}

```

* In the newest version of T-reqs, user stories should be stored in other tools like Scrum boards or Kanban boards. Then, only their IDs can be referenced by other T-reqs artifacts. For example, the [requirement id=REQ0001 story=US0001] indicates that a system requirement with ID `REQ0001` references a user story of ID `US0001`.

The formats are here mainly chosen for convenience, but can easily be adapted to any company standard.

## Further reading:

- [Requirements Challenges in Large-Scale Agile](https://oerich.wordpress.com/2017/06/28/re-for-large-scale-agile-system-development/)
- [T-Reqs: Key idea and User Stories](https://arxiv.org/abs/1805.02769)
