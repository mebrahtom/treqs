---
name: User story
about: Define a concrete user need
Acceptance cases: Please write the acceptance cases 
that need to be fulfilled for the userstory implementation to be accepted.
---
[userstory id=USX]

_As a_ [role] _I want to_ goal _so that_ value.
