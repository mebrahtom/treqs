from setuptools import setup, find_packages

setup(name='treqs',
      version='2.0',
      description='Tool Support for Managing Requirements in Large-Scale Agile System Development',
      url='https://gitlab.com/mebrahtom/treqs/tree/dev',
      author='Mebrahtom',
      author_email='mebrinno@gmail.com',
      license='MIT',
      packages=find_packages(),
      entry_points={
          'console_scripts': [
              'treqs=treqs.main:call_main',
              'generatepreview=treqs.generatePreview:generate_preview',
              'generateid=treqs.generateId:get_user_input',
              'treqsconfig=treqs.manageConfig:manage_config',
              'generatedid=treqs.idHistory:print_generated_ids'
          ],
      },
      install_requires=['requests', 'python-gitlab',
                        'gitpython', 'pydriller', 'mysql-connector-python'],
      zip_safe=False)
