#!/usr/bin/env python3
import getopt
import os
import re
import sys
import datetime


class QRProcessor:

    def __init__(self):
        self.log = open(
            'logs/QR_log_'+datetime.datetime.now().strftime("%Y%m%d%H%M%S")+'.md', "w")

    # Sets for all ids
    reqSet = set()
    noReqTracingSet = set()
    qrIDSet = set()
    duplicateIDSet = set()

    # NOTE: Not self-contained/effect-free. It uses and mutates the sets in which ids are stored (storySet,noUSTracingSet,reqSet,noReqTracingSet,testIDSet,duplicateIDSet)
    def processQRLine(self, line):
        "This processes a single line in a Gherkin feature file, extracting duplicate IDs, missing traces, and a list of all traces to US."

        success = True
        # Extracts the actual test case tag if there is one. Note that this requires the tag to be in a single line.
        m = re.search(r'\[quality .*?\]', line)
        if m:
            self.log.write('New Quality Requirement:\n')
            reqtag = m.group(0)
            self.log.write(reqtag+'\n')

            # Extract the id attribute from within the test case tag. Only test cases with id are processed.
            id = re.search(r'(?<=id=).*?(?=[ \]])', reqtag)
            if id:
                id = id.group(0)
                self.log.write(id+'\n')

                # Find duplicate ids. Note that currently, duplicate ids are still processed further.
                if id in self.qrIDSet:
                    self.log.write('Duplicate QR id:'+id+'\n')
                    self.duplicateIDSet.add(id)
                    success = False
                else:
                    self.qrIDSet.add(id)

                # Find all story attributes. Supports also multiple story attributes in theory.
                reqs = re.findall(r'(?<=requirement=).*?(?=[ \]])', reqtag)

                # Find test cases without user story traces
                if len(reqs) == 0:
                    self.log.write(
                        'Warning: Quality requirement is not traced to a system requirement!\n')
                    self.noReqTracingSet.add(id)
                    success = False
                else:
                    for currentReq in reqs:
                        # Support potential commas in an issue attribute
                        splitreqs = re.split(',', currentReq)
                        for req in splitreqs:
                            self.log.write(req+'\n')
                            self.reqSet.add(req)

            self.log.write('\n')
        return success

    def processAllQR(self, dir, recursive, filePattern=r'QR_.*?(.md)'):

        success = True

        # recursive traversion of root directory
        if recursive:
            for root, directories, filenames in os.walk(dir):
                #	for directory in directories:
                #		self.log.write os.path.join(root, directory)
                for filename in filenames:
                    # Only files matching the given pattern are scanned
                    match = re.search(filePattern, filename)
                    if match:
                        with open(os.path.join(root, filename), "r", encoding='utf-8') as file:
                            for line in file:
                                success = self.processQRLine(
                                    line) and success
        else:
            listOfFiles = os.listdir(dir)
            # Only files matching the given pattern are scanned
            for f in listOfFiles:
                match = re.search(filePattern, f)
                if match:
                    with open(os.path.join(dir, f), "r") as file:
                        for line in file:
                            success = self.processQRLine(
                                line) and success

        # Simple self.log.writeouts of all relevant sets.
        self.log.write('All quality requirement traces:\n')
        for currentRequirement in self.reqSet:
            self.log.write(currentRequirement+'\n')
        self.log.write('\n')

        self.log.write(
            'Quality requirements without traces to system requirements:\n')
        for currentID in self.noReqTracingSet:
            self.log.write(currentID+'\n')
        self.log.write('\n')

        self.log.write('Duplicate Quality Requirement IDs:\n')
        for currentID in self.duplicateIDSet:
            self.log.write(currentID+'\n')

        self.log.close()
        return success
