#!/usr/bin/env python3

import getopt
import sys
import datetime
import os
import re
import json
import gitlab
import requests
import git
import boto3
from treqs import idHistory
from treqs import dataEncDec
from treqs import readConfig
from treqs import mUSProcessor, mSysReqProcessor, mTCProcessor, mATProcessor, mQRProcessor


def main(argv):
    repo = git.Repo('.', search_parent_directories=True)
    path = repo.working_tree_dir
    path = os.path.normpath(path)
    data= readConfig.readConfig()
    artifactsDir = path+"/"+data["artsDir"]
    artifacts = data["artifacts"]
    sysReq = artifacts["sysReq"]
    qualityReq = artifacts["qualityReq"]
    testCase = artifacts["testCase"]
    acceptanceTest = artifacts["acceptanceTest"]
    userStory = artifacts["userStory"]
    usPattern = userStory["fileNamePattern"]
    usIdPrefix = userStory["idPrefix"]
    tcPattern = testCase["fileNamePattern"]
    tcIdPrefix = testCase["idPrefix"]
    sysReqPattern = sysReq["fileNamePattern"]
    sysReqIdPrefix = sysReq["idPrefix"]
    atPattern = acceptanceTest["fileNamePattern"]
    atIdPrefix = acceptanceTest["idPrefix"]
    qrPattern = qualityReq["fileNamePattern"]
    qrIdPrefix = qualityReq["idPrefix"]
    treqsDB = data["treqsDB"]
    hostName = treqsDB["hostName"]
    userName = treqsDB["userName"]
    userPass = treqsDB["userPass"]
    dbName = treqsDB["dbName"]
    portNumber = treqsDB["portNumber"]
    recursive = True
    quiet = False

    try:
        os.makedirs('logs')
    except OSError:
        if not os.path.isdir('logs'):
            raise

    # Do all the data processing here
    reqProcessor = mSysReqProcessor.SysReqsProcessor()
    success = reqProcessor.processAllLines(
        artifactsDir, recursive, sysReqPattern)

    usProcessor = mUSProcessor.USProcessor()
    success = usProcessor.processAllUS(
        artifactsDir, recursive, usPattern) and success

    tcProcessor = mTCProcessor.TCProcessor()
    success = tcProcessor.processAllTC(
        artifactsDir, recursive, tcPattern) and success
    atProcessor = mATProcessor.ATProcessor()
    success = atProcessor.processAllAT(
        artifactsDir, recursive, atPattern) and success

    qrProcessor = mQRProcessor.QRProcessor()
    success = qrProcessor.processAllQR(
        artifactsDir, recursive, qrPattern) and success
    session = boto3.session.Session()
    userName = dataEncDec.decryptData(session, userName)
    userPass = dataEncDec.decryptData(session, userPass)
    generatedUSIDs = idHistory.get_generated_ids("us",hostName,portNumber,userName,userPass,dbName)
    generatedReqIDs = idHistory.get_generated_ids("sr",hostName,portNumber,userName,userPass,dbName)
    generatedQReqIDs = idHistory.get_generated_ids("qr",hostName,portNumber,userName,userPass,dbName)
    generatedTestIDs = idHistory.get_generated_ids("tc",hostName,portNumber,userName,userPass,dbName)
    generatedAccTestIDs = idHistory.get_generated_ids("at",hostName,portNumber,userName,userPass,dbName)
    # Get all the user stories and traces to them
    existingStories = usProcessor.storySet
    nonUsedUSIDs = generatedUSIDs.difference(existingStories)
    tracedStoriesFromReqs = reqProcessor.storySet
    tracedStoriesFromTests = tcProcessor.storySet
    tracedStoriesFromAccepts = atProcessor.storySet
    # Calculate which story IDs are not traced to
    diffUSReq = existingStories.difference(tracedStoriesFromReqs)
    diffUSTests = existingStories.difference(tracedStoriesFromTests)
    diffUSAccepts = existingStories.difference(tracedStoriesFromAccepts)
    # Calculate whether there are any traces to non-existing stories
    nonExistingUSReqs = tracedStoriesFromReqs.difference(existingStories)
    nonExistingUSTests = tracedStoriesFromTests.difference(existingStories)
    nonExistingUSAccepts = tracedStoriesFromAccepts.difference(existingStories)

    # Get all the requirements and traces to them
    existingReqs = reqProcessor.reqIDSet
    nonUsedReqIDs = generatedReqIDs.difference(existingReqs)
    tracedReqsFromTests = tcProcessor.reqSet
    # Calculate which Req IDs are not traced to
    diffReqTests = existingReqs.difference(tracedReqsFromTests)
    # Calculate which Req IDs are traced to but do not exist
    nonExistingReqTC = tracedReqsFromTests.difference(existingReqs)

    # Get all quality requirements and traces to them
    existingQReqs = qrProcessor.qrIDSet
    tracedReqsFromQReqs = qrProcessor.reqSet
    # Calculate which Req IDs are traced to but do not exist
    nonExistingReqQR = tracedReqsFromQReqs.difference(existingReqs)
    # Get non-used quality requirement IDs
    nonUsedQReqIDs = generatedQReqIDs.difference(existingQReqs)
    # Get all duplicate IDs
    duplicateUS = usProcessor.duplicateStorySet
    duplicateTC = tcProcessor.duplicateIDSet
    duplicateReq = reqProcessor.duplicateIDSet
    duplicateAT = atProcessor.duplicateIDSet
    duplicateQR = qrProcessor.duplicateIDSet
    # Get all IDs of artifacts missing traces
    reqsWithoutUSTraces = reqProcessor.noUSTracingSet
    testsWithoutUSTraces = tcProcessor.noUSTracingSet
    testsWithoutReqTraces = tcProcessor.noReqTracingSet
    acceptsWithoutUSTraces = atProcessor.noUSTracingSet
    qReqsWithoutReqTraces = qrProcessor.noReqTracingSet
    # Get all test case and acceptance test IDs
    existingTests = tcProcessor.testIDSet
    existingAccTests = atProcessor.testIDSet
    # Get all non-used test case and acceptance test IDs
    nonUsedAccTestIDs = generatedAccTestIDs.difference(existingAccTests)
    nonUsedTestIDs = generatedTestIDs.difference(existingTests)
    if success and (len(nonExistingUSReqs) > 0 or len(nonExistingUSTests) > 0 or len(nonExistingReqTC) > 0):
        success = False

    filename = 'logs/Summary_log_'+datetime.datetime.now().strftime("%Y%m%d%H%M%S")+'.md'
    log = open(filename, "w")
    log.write('# T-Reqs commit report #\n\n')
    if len(duplicateUS) > 0 or len(duplicateReq) > 0 or len(duplicateQR) > 0 or len(duplicateAT) > 0 or len(duplicateQR) > 0:
        log.write('### Duplicate IDs ###\n')
    if len(duplicateUS) > 0:
        log.write('The following duplicate User Story IDs exist:\n\n')
        for currentID in duplicateUS:
            log.write('* User Story ' + currentID + '\n')
        log.write('\n')

    if len(duplicateReq) > 0:
        log.write('The following duplicate System Requirement IDs exist:\n\n')
        for currentID in duplicateReq:
            log.write('* System Requirement ' + currentID + '\n')
        log.write('\n')

    if len(duplicateTC) > 0:
        log.write('The following duplicate Test Case IDs exist:\n\n')
        for currentID in duplicateTC:
            log.write('* Test Case ' + currentID + '\n')
        log.write('\n')

    if len(duplicateAT) > 0:
        log.write('The following duplicate Acceptance Test IDs exist:\n\n')
        for currentID in duplicateAT:
            log.write('* Acceptance Test ' + currentID + '\n')
        log.write('\n')

    if len(duplicateQR) > 0:
        log.write('The following duplicate Quality Requirement IDs exist:\n\n')
        for currentID in duplicateQR:
            log.write('* Quality Requirement ' + currentID + '\n')
        log.write('\n')
    if len(reqsWithoutUSTraces) > 0 or len(testsWithoutUSTraces) > 0 or len(testsWithoutReqTraces) > 0 or len(acceptsWithoutUSTraces) > 0 or len(qReqsWithoutReqTraces) > 0 or len(nonUsedTestIDs) > 0:
        log.write('### Items without traces ###\n')

    if len(reqsWithoutUSTraces) > 0:
        log.write(
            'The following System Requirements lack traces to user stories:\n\n')
        for currentID in reqsWithoutUSTraces:
            log.write('* System Requirement ' + currentID + '\n')
        log.write('\n')

    if len(testsWithoutUSTraces) > 0:
        log.write('The following Test Cases lack traces to user stories:\n\n')
        for currentID in testsWithoutUSTraces:
            log.write('* Test Case ' + currentID + '\n')
        log.write('\n')

    if len(testsWithoutReqTraces) > 0:
        log.write(
            'The following Test Cases lack traces to system requirements:\n\n')
        for currentID in testsWithoutReqTraces:
            log.write('* Test Case ' + currentID + '\n')
        log.write('\n')

    if len(acceptsWithoutUSTraces) > 0:
        log.write('The following Acceptance Tests lack traces to user stories:\n\n')
        for currentID in acceptsWithoutUSTraces:
            log.write('* Acceptance Test ' + currentID + '\n')
        log.write('\n')

    if len(qReqsWithoutReqTraces) > 0:
        log.write(
            'The following quality requirements lack traces to system requirements:\n\n')
        for currentID in qReqsWithoutReqTraces:
            log.write('* Quality Requirement ' + currentID + '\n')
        log.write('\n')
    if len(diffUSReq) > 0 or len(diffUSTests) > 0 or len(diffReqTests) > 0 or len(diffUSAccepts) > 0:
        log.write('### Missing traces ###\n')

    if len(diffUSReq) > 0:
        log.write(
            'The following user stories are not referenced by any system requirement:\n\n')
        for currentID in diffUSReq:
            log.write('* User Story ' + currentID + '\n')
        log.write('\n')

    if len(diffUSTests) > 0:
        log.write(
            'The following user stories are not referenced by any test case:\n\n')
        for currentID in diffUSTests:
            log.write('* User Story ' + currentID + '\n')
        log.write('\n')

    if len(diffReqTests) > 0:
        log.write(
            'The following system requirements are not referenced by any test case:\n\n')
        for currentID in diffReqTests:
            log.write('* System Requirement ' + currentID + '\n')
        log.write('\n')

    if len(diffUSAccepts) > 0:
        log.write(
            'The following user stories are not referenced by any acceptance test:\n\n')
        for currentID in diffUSAccepts:
            log.write('* Acceptance Test ' + currentID + '\n')
        log.write('\n')
    if len(nonExistingUSReqs) > 0 or len(nonExistingUSReqs) > 0 or len(nonExistingUSTests) > 0 or len(nonExistingReqTC) > 0 or len(nonExistingUSAccepts) > 0 or len(nonExistingReqQR) > 0:
        log.write('### Missing items ###\n')

    if len(nonExistingUSReqs) > 0:
        log.write(
            'The following user stories are referenced by requirements, but do not exist:\n\n')
        for currentID in nonExistingUSReqs:
            log.write('* User Story ' + currentID + '\n')
        log.write('\n')

    if len(nonExistingUSTests) > 0:
        log.write(
            'The following user stories are referenced by test cases, but do not exist:\n\n')
        for currentID in nonExistingUSTests:
            log.write('* User Story ' + currentID + '\n')
        log.write('\n')

    if len(nonExistingReqTC) > 0:
        log.write(
            'The following requirements are referenced by test cases, but do not exist:\n\n')
        for currentID in nonExistingReqTC:
            log.write('* Requirement ' + currentID + '\n')
        log.write('\n')

    if len(nonExistingUSAccepts) > 0:
        log.write(
            'The following userstories are referenced by acceptance tests, but do not exist:\n\n')
        for currentID in nonExistingUSAccepts:
            log.write('* Userstory ' + currentID + '\n')
        log.write('\n')

    if len(nonExistingReqQR) > 0:
        log.write(
            'The following system requirements are referenced by quality requirements, but do not exist:\n\n')
        for currentID in nonExistingReqQR:
            log.write('* Requirement ' + currentID + '\n')
    if len(nonUsedReqIDs) > 0 or len(nonUsedQReqIDs) > 0 or len(nonUsedUSIDs) > 0 or len(nonUsedAccTestIDs) > 0 or len(nonUsedTestIDs) > 0:
        log.write('### Unused IDs ###\n')
    if len(nonUsedReqIDs) > 0:
        log.write("The following system requirement IDs are not used:\n\n")
        for nonUsedID in sorted(nonUsedReqIDs):
            log.write(nonUsedID + '\n')
    if len(nonUsedQReqIDs) > 0:
        log.write("The following quality requirement IDs are not used:\n\n")
        for nonUsedID in sorted(nonUsedQReqIDs):
            log.write(nonUsedID + '\n')
    if len(nonUsedUSIDs) > 0:
        log.write("The following user story IDs are not used:\n\n")
        for nonUsedID in sorted(nonUsedUSIDs):
            log.write(nonUsedID + '\n')
    if len(nonUsedAccTestIDs) > 0:
        log.write("The following acceptance test IDs are not used:\n\n")
        for nonUsedID in sorted(nonUsedAccTestIDs):
            log.write(nonUsedID + '\n')
    if len(nonUsedTestIDs) > 0:
        log.write("The following test IDs are not used:\n\n")
        for nonUsedID in sorted(nonUsedTestIDs):
            log.write(nonUsedID + '\n')
    for req_id in existingReqs:
        if not re.match(sysReqIdPrefix + r"[0-9]{4}", req_id):
            log.write(req_id +
                      " doesn't follow the system requirement ID prefix set in configuration file\n")
    for qr_id in existingQReqs:
        if not re.match(qrIdPrefix + r"[0-9]{4}", qr_id):
            log.write(qr_id +
                      " doesn't follow the quality requirement ID prefix set in configuration file\n")
    for us_id in existingStories:
        if not re.match(usIdPrefix + r"[0-9]{4}", us_id):
            log.write(us_id +
                      " doesn't follow the user story ID prefix set in configuration file\n")
    for tc_id in existingTests:
        if not re.match(tcIdPrefix + r"[0-9]{4}", tc_id):
            log.write(tc_id +
                      " doesn't follow the test case ID prefix set in configuration file\n")
    for at_id in existingAccTests:
        if not re.match(atIdPrefix + r"[0-9]{4}", at_id):
            log.write(at_id +
                      " doesn't follow the acceptance test ID prefix set in configuration file\n")
    log.close()
    # usProcessor.generate_issue_stories(currentPath, artifactsDir, projectId)
    # Return -1 if the validation has failed, and print the log to the console
    if not success and not quiet:
        print('Validation failed with the following output:\n')
        with open(filename, 'r') as f:
            print(f.read())
        return -1


def call_main():
    main(sys.argv)
