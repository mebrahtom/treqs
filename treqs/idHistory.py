import sys
import os
import json
import boto3
import argparse
import mysql.connector
from datetime import datetime
from git import Repo
from treqs import readConfig
from treqs import dataEncDec

repo = Repo('.', search_parent_directories=True)
path = repo.working_tree_dir
path = os.path.normpath(path)
reader = repo.config_reader()
email = reader.get_value("user", "email")


def print_generated_ids():
    data = readConfig.readConfig()
    treqsDB = data["treqsDB"]
    hostName = treqsDB["hostName"]
    userName = treqsDB["userName"]
    userPass = treqsDB["userPass"]
    dbName = treqsDB["dbName"]
    portNumber = treqsDB["portNumber"]
    parser = argparse.ArgumentParser(
        description='ID generator for treqs')
    parser.add_argument('--artifact', '-a', type=str, required=True,
                        help="Type of artifact to which the ID belongs")
    parser.add_argument('--date', '-d', type=str,
                        help="The date on which the ID was generated")

    args = parser.parse_args()
    conn = None
    dash = '-' * 25
    session = boto3.session.Session()
    userName = dataEncDec.decryptData(session, userName)
    userPass = dataEncDec.decryptData(session, userPass)
    if args.artifact:
        artifactType = args.artifact
        if args.date:
            date_generated = args.date
        else:
            date_generated = "all"
        try:
            conn = mysql.connector.connect(
                host=hostName, port=portNumber, user=userName, passwd=userPass, database=dbName)
            if conn.is_connected:
                cursor = conn.cursor()
                if artifactType == "sr":
                    if date_generated == "all":
                        select_query = """SELECT sysreqid, date_generated FROM sysreqid_tbl WHERE user_email=%s"""
                    else:
                        select_query = """SELECT sysreqid, date_generated FROM sysreqid_tbl WHERE date_generated=%s AND user_email=%s"""
                elif artifactType == "qr":
                    if date_generated == "all":
                        select_query = """SELECT qualityreqid, date_generated FROM qualityreqid_tbl WHERE user_email=%s"""
                    else:
                        select_query = """SELECT qualityreqid, date_generated FROM qualityreqid_tbl WHERE date_generated = %s AND user_email = %s"""
                elif artifactType == "us":
                    if date_generated == "all":
                        select_query = """SELECT userstoryid, date_generated FROM userstoryid_tbl WHERE user_email=%s"""
                    else:
                        select_query = """SELECT userstoryid, date_generated FROM userstoryid_tbl WHERE date_generated=%s AND user_email=%s"""
                elif artifactType == "tc":
                    if date_generated == "all":
                        select_query = """SELECT testid, date_generated FROM testid_tbl WHERE user_email=%s"""
                    else:
                        select_query = """SELECT testid, date_generated FROM testid_tbl WHERE date_generated=%s AND user_email=%s"""
                elif artifactType == "at":
                    if date_generated == "all":
                        select_query = """SELECT acceptancetestid, date_generated FROM acceptancetestid_tbl WHERE user_email=%s"""
                    else:
                        select_query = """SELECT acceptancetestid, date_generated FROM acceptancetestid_tbl WHERE date_generated=%s AND user_email=%s"""
                else:
                    print("Command option not known!")
                    sys.exit(0)
                if date_generated == "all":
                    cursor.execute(select_query, (email,))
                    rows = cursor.fetchall()
                    if rows:
                        print("You have generated the following IDs!")
                        print(dash)
                        print("{:<10s}{:>10s}".format("ID", "Generation date"))
                        print(dash)
                    else:
                        print(
                            "You have not generated IDs with the email address set in your current git installation!")
                        sys.exit(0)
                    for row in rows:
                        print("{:<10s}{:>10s}".format(row[0], str(row[1])))
                else:
                    cursor.execute(select_query, (date_generated, email))
                    rows = cursor.fetchall()
                    if rows:
                        print(
                            "You have generated the following IDs on " + date_generated + ":")
                        print(dash)
                        print("{:<10s}{:>10s}".format("ID", "Generation date"))
                        print(dash)
                        for row in rows:
                            print("{:<10s}{:>10s}".format(row[0], str(row[1])))
                    else:
                        print(
                            "You have not generated IDs with the email address set in your git installation!")
                        sys.exit(0)

        except Exception as e:
            print("Database connectivity error!", e)
            sys.exit(0)
        finally:
            if conn is not None:
                cursor.close()
                conn.close()


def get_generated_ids(artifactType, hostName, portNumber, userName, userPass, dbName):
    conn = None
    try:
        conn = mysql.connector.connect(
            host=hostName, port=portNumber, user=userName, passwd=userPass, database=dbName)
        if conn.is_connected:
            cursor = conn.cursor()
            generatedIDs = set()
            if artifactType == "sr":
                select_query = """SELECT sysreqid FROM sysreqid_tbl WHERE user_email=%s"""
            elif artifactType == "qr":
                select_query = """SELECT qualityreqid FROM qualityreqid_tbl WHERE user_email=%s"""
            elif artifactType == "us":
                select_query = """SELECT userstoryid FROM userstoryid_tbl WHERE user_email=%s"""
            elif artifactType == "tc":
                select_query = """SELECT testid FROM testid_tbl WHERE user_email=%s"""
            elif artifactType == "at":
                select_query = """SELECT acceptancetestid FROM acceptancetestid_tbl WHERE user_email=%s"""
            else:
                print("Command option not known!")
                sys.exit(0)
            cursor.execute(select_query, (email,))
            rows = cursor.fetchall()
            for row in rows:
                for artifact_id in row:
                    generatedIDs.add(artifact_id)
            return generatedIDs
    except Exception as e:
        print("Database connectivity error!", e)
        sys.exit(0)
    finally:
        if conn is not None:
            cursor.close()
            conn.close()
