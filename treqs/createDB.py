import sys
import mysql.connector


def create_db(hostName, userName, userPass, dbName, portNumber):
    """Prepares the database for storing IDs.

    Creates the treqsdb database if it doesn't exist.
    Creates the necessary tables within the treqsdb database.
    """
    conn = None
    try:
        conn = mysql.connector.connect(
            host=hostName, port=portNumber, user=userName, passwd=userPass)
        if conn.is_connected:
            cursor = conn.cursor()
            cursor.execute("""CREATE DATABASE IF NOT EXISTS """ + dbName)
            conn = mysql.connector.connect(
                host=hostName, port=portNumber, user=userName, passwd=userPass, database=dbName)
            if conn.is_connected:
                cursor = conn.cursor()
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS sysreqid_tbl(id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, sysreqid VARCHAR(20) UNIQUE NOT NULL, user_email VARCHAR(60), date_generated date)""")
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS qualityreqid_tbl(id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, qualityreqid VARCHAR(20) UNIQUE NOT NULL, user_email VARCHAR(60), date_generated date)""")
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS testid_tbl(id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, testid VARCHAR(20) UNIQUE NOT NULL, user_email VARCHAR(60), date_generated date)""")
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS userstoryid_tbl(id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, userstoryid VARCHAR(20) UNIQUE NOT NULL, user_email VARCHAR(60), date_generated date)""")
                cursor.execute(
                    """CREATE TABLE IF NOT EXISTS acceptancetestid_tbl(id INT AUTO_INCREMENT NOT NULL PRIMARY KEY, acceptancetestid VARCHAR(20) UNIQUE NOT NULL, user_email VARCHAR(60), date_generated date)""")
    except Exception as e:
        print("Database connectivity error!", e)
        sys.exit(0)
    finally:
        if conn is not None:
            cursor.close()
            conn.close()
