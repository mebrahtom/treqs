import base64
import boto3


def decryptData(session, encrypted):
    client = session.client('kms')
    plaintext = client.decrypt(
        CiphertextBlob=base64.b64decode(encrypted))
    return plaintext[u"Plaintext"].decode()


def encryptData(session, data, alias):
    client = session.client('kms')
    encrypted = client.encrypt(KeyId=alias, Plaintext=data)
    return base64.b64encode(encrypted[u'CiphertextBlob']).decode()
