"""
Some of the following code is taken from
https://github.com/dougn/python-plantuml and modified to
fit the implementation of this project.
"""
import zlib
import os
import sys
import json
import re
from git import Repo
reqIDSet = set()
repo = Repo('.', search_parent_directories=True)
path = repo.working_tree_dir
path = os.path.normpath(path)
configFileExists = False
for root, dirs, files in os.walk(path):
    for file in files:
        filePath = os.path.join(root, file)
        if file == ".config.json" and os.path.dirname(os.path.dirname(filePath)) == path:
            with open(filePath, "r") as f:
                data = json.load(f)
                repoManager = data['repoManager']
                groupName = data['groupName']
                artsDir = path+"/"+data['artsDir']
            configFileExists = True
            break

if configFileExists == False:
    print("Please configure your working environment with the treqsconfig command!")
    sys.exit(0)
#: Default plantuml service url
SERVER_URL = 'http://www.plantuml.com/plantuml/svg/'


def deflate_and_encode(plantuml_text):
    """zlib compress the plantuml text and encode it for the plantuml server.
    """
    zlibbed_str = zlib.compress(plantuml_text.encode('utf-8'))
    compressed_string = zlibbed_str[2:-4]
    return encode(compressed_string.decode('latin-1'))


def encode(data):
    """encode the plantuml data which may be compresses in the proper
    encoding for the plantuml server
    """
    res = ""
    for i in range(0, len(data), 3):
        if (i+2 == len(data)):
            res += _encode3bytes(ord(data[i]), ord(data[i+1]), 0)
        elif (i+1 == len(data)):
            res += _encode3bytes(ord(data[i]), 0, 0)
        else:
            res += _encode3bytes(ord(data[i]), ord(data[i+1]), ord(data[i+2]))
    return res


def _encode3bytes(b1, b2, b3):
    c1 = b1 >> 2
    c2 = ((b1 & 0x3) << 4) | (b2 >> 4)
    c3 = ((b2 & 0xF) << 2) | (b3 >> 6)
    c4 = b3 & 0x3F
    res = ""
    res += _encode6bit(c1 & 0x3F)
    res += _encode6bit(c2 & 0x3F)
    res += _encode6bit(c3 & 0x3F)
    res += _encode6bit(c4 & 0x3F)
    return res


def _encode6bit(b):
    if b < 10:
        return chr(48 + b)
    b -= 10
    if b < 26:
        return chr(65 + b)
    b -= 26
    if b < 26:
        return chr(97 + b)
    b -= 26
    if b == 0:
        return '-'
    if b == 1:
        return '_'
    return '?'


def get_url(plantuml_text):
    """Return the server URL for the image.
    You can use this URL in an IMG HTML tag.

    :param str plantuml_text: The plantuml markup to render
    :returns: the plantuml server image URL
    """
    return SERVER_URL + deflate_and_encode(plantuml_text)


def generate_preview():
    """Generates a URL for PlantUML diagram.
    """
    md_files = set()
    if len(sys.argv) > 1:
        for file in sys.argv[1:]:
            md_files.add(file)
    else:
        stagedFiles = repo.index.diff("HEAD")
        for file in stagedFiles:
            filePath = file.a_path
            if filePath.endswith(".md"):
                md_files.add(filePath)
    for md_file in md_files:

        with open(md_file, "r", encoding="utf-8") as file:
            file_content = file.read()
            # Remove old trace links from PlantUML code
            file_content = re.sub(
                r'\s+' + re.escape('[[') + '.*?[0-9]{4}' + re.escape(']]'), "", file_content, re.DOTALL)
            # Remove old trace links from DOT language
            file_content = re.sub(
                r'\s+' + '.*?[0-9]{4}' + re.escape('[') + '.*?' + re.escape(']'), "", file_content, re.DOTALL)
            # Remove old generated diagram URL
            file_content = re.sub(r'\s+' + re.escape('![') + 'Generated diagram' + re.escape(
                ']') + '.*?' + re.escape(')'), "", file_content, re.DOTALL)
            file_content = write_trace_links(file_content)
            # Extract PlantUML code blocks
            puml_content = re.findall(
                r'```puml(.*?)```', file_content, re.DOTALL)
            # Generate the digram URL for each PlantUML code block
        for puml in puml_content:
            diagram_url = get_url(puml)
            puml_with_url = '```puml'+puml + '```\n' +\
                "\n![Generated diagram]({})".format(diagram_url)
            file_content = file_content.replace(
                '```puml' + puml + '```', puml_with_url)

        with open(md_file, "w") as file:
            file.write(file_content)
        repo.index.add(([md_file]))


def write_trace_links(file_content):
    """Writes the generated trace links within the rest of the file content.

    Args:
        file_content: The content of the file before the trace links are generated.

    Returns:
          The content of the file updated with the generated trace links.
    """
    for line in file_content.splitlines():
        m = re.search(
            re.escape('[') + 'requirement .*?' + re.escape(']'), line)
        if m:
            reqtag = m.group(0)
            id = re.search(r'id\s?=\s?(.*?)\s', reqtag)
            if id:
                id = id.group(1)
                if id in reqIDSet:
                    print('Duplicate requirement id:' + id + '\n')
                else:
                    reqIDSet.add(id)
                    qualityLinks = str()
                    testLinks = str()
                    storyLinks = str()
                    storyTraceLinks = {}
                    testTraceLinks = {}
                    qualityTraceLinks = {}
                    stories = re.findall(
                        r'(?<=story=).*?(?=[ \]])', reqtag)
                    tests = re.findall(
                        r'(?<=test=).*?(?=[ \]])', reqtag)
                    qualities = re.findall(
                        r'(?<=quality=).*?(?=[ \]])', reqtag)
                    if len(stories) == 0:
                        print(
                            "Warning: Requirement "+id+" is not traced to a user story \n")
                    else:
                        storyTraceLinks = generate_trace_links(
                            stories, "userstory")
                        if line.strip() == "/'" + reqtag + "'/":
                            for storyId, storyLink in storyTraceLinks.items():
                                storyLinks = storyLinks + \
                                    "\n[[" + storyLink + " " + storyId + "]]"
                        if line.strip() == "/*" + reqtag + "*/":
                            for storyId, storyLink in storyTraceLinks.items():
                                storyLinks = storyLinks + \
                                    "\n"+storyId + \
                                    "[shape=note,fontcolor = \"#0000FF\", style = \"filled\", color = \"#A80035\", fillcolor=\"#FBFB77\", href=\"" + storyLink+"\"]"

                    if len(tests) == 0:
                        print(
                            "Warning: Requirement "+id+" is not traced to test case \n")
                    else:
                        testTraceLinks = generate_trace_links(
                            tests, "testcase")
                        if line.strip() == "/'" + reqtag + "'/":
                            for testId, testLink in testTraceLinks.items():
                                testLinks = testLinks + "\n[[" + \
                                    testLink + " " + testId + "]]"
                        if line.strip() == "/*" + reqtag + "*/":
                            for testId, testLink in testTraceLinks.items():
                                testLinks = testLinks + \
                                    "\n"+testId + \
                                    "[shape=note,fontcolor = \"#0000FF\", style = \"filled\", color = \"#A80035\", fillcolor=\"#FBFB77\", href=\"" + testLink + "\"]"
                    if len(qualities) == 0:
                        print(
                            "Warning: Requirement "+id+" is not traced to quality requirements \n")
                    else:
                        qualityTraceLinks = generate_trace_links(
                            qualities, "quality")
                        if line.strip() == "/'" + reqtag + "'/":
                            for qualityId, qualityLink in qualityTraceLinks.items():
                                qualityLinks = qualityLinks + "\n[[" + \
                                    qualityLink + " " + qualityId + "]]"
                        if line.strip() == "/*" + reqtag + "*/":
                            for qualityId, qualityLink in qualityTraceLinks.items():
                                qualityLinks = qualityLinks + \
                                    "\n"+qualityId + \
                                    "[shape=note,fontcolor = \"#0000FF\", style = \"filled\", color = \"#A80035\", fillcolor=\"#FBFB77\", href=\"" + qualityLink+"\"]"
                    if line.strip() == "/'" + reqtag + "'/":
                        links = "/'" + reqtag + "'/" + testLinks + qualityLinks + storyLinks
                        file_content = file_content.replace(
                            "/'" + reqtag + "'/", links)
                    if line.strip() == "/*" + reqtag + "*/":
                        links = "/*" + reqtag + "*/" + testLinks + qualityLinks + storyLinks
                        file_content = file_content.replace(
                            "/*" + reqtag + "*/", links)

    return file_content


def generate_trace_links(artifact_category, tag_name):
    """Generates tracelinks.
    Args:
        artifact_category: Traces of the same category. For example, all testcase traces.
        tag_name: The tag name that identifies an artifact. For example, userstory.

    Returns:
            A dictionary of tracelinks
    """
    artifactTraceLinks = {}
    for artifact in artifact_category:
        splitartifact = re.split(',', artifact)
        for currentArtifact in splitartifact:
            for dirpath, dirs, files in os.walk(artsDir):
                if "__pycache__" in dirs:
                    dirs.remove("__pycache__")
                if "logs" in dirs:
                    dirs.remove("logs")
                for file in files:
                    filePath = os.path.join(
                        dirpath, file)
                    with open(filePath, encoding='utf-8') as file:
                        for line in file:
                            m = re.search(
                                r'\[\s?'+re.escape(tag_name)+r'\s?id\s?=(.*?)(\s|\])', line)
                            if m:
                                artifactFound = m.group(
                                    1)
                                if currentArtifact.strip() == artifactFound.strip():
                                    activeBranch = repo.active_branch
                                    branchName = activeBranch.name
                                    repoName = os.path.basename(
                                        path)
                                    relativePath = os.path.relpath(
                                        filePath)
                                    formattedPath = relativePath.replace(
                                        os.sep, '/')
                                    artifactTraceLink = (repoManager+"/"+groupName+"/"
                                                         + repoName+"/blob/"+branchName+"/"+formattedPath)
                                    artifactTraceLinks[artifactFound] = artifactTraceLink
    return artifactTraceLinks
