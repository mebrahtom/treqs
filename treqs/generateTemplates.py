import os


def create_sys_req_template(path):
    with open(path + "/template_textual_ sys_reqs.md", "w") as file:
        file.write(
            "# Document title\n\nDocument description\n\n" +
            "## Subsection\n\nSection description\n\n" +
            "### [requirement id=REQ0001 story=US0005 test=TC0007 quality=QR0006]\n\n" +
            "Requirement text\n\n## Another subsection\n\n" +
            "Section description\n\n" +
            "### [requirement id=REQ0002 story=US0002,US0005 quality=QR0007 test=TC0007,TC0008]\n\n" +
            "Requirement text\n"
        )

    with open(path + "/template_model_ sys_reqs.md", "w") as file:
        file.write(
            "# Writing model-based system requirements using PlantUML/DOT language\n\n" +
            "## PlantUML diagram\n\nSome text\n\n```puml\n@startuml\n" +
            "/'PlantUML code goes here'/\n" +
            "/'Traceability information should be written with PlantUML notes as indicated in the following line'/\n" +
            "note right|left|bottom|top on <element>\n" +
            "/'[requirement id=SR0001 test=TC0002,TC0005 quality=QR0010]'/\n" +
            "end note\n\n@enduml\n```\n\nSome other text\n\n## DOT graph\n\n" +
            "Some text\n\n```puml\n@startdot\n/*DOT language goes here*/\n" +
            "/*Traceability information should be specified in subgraphs as indicated in the following lines*/\n\n" +
            "digraph <digraph_name>{\n/*Some code*/\n    subgraph <subgraph_name>{\n" +
            "    //subgraph code\n    /*[requirement id=SR0001 test=TC0002,TC0005 quality=QR0010]*/\n    //some more code\n"
            "  }\n}\n\n@enddot\n```\n\nSome other text\n"
        )


def create_test_case_template(path):
    with open(path + "/template_manual_test_case.md", "w") as file:
        file.write(
            "# Document title\n\nDocument description\n\n" +
            "## [testcase id=TC0001 story=US0001 requirement=REQ0001]" +
            "\n\nPurpose: Purpose of the test case without line break.\n\n" +
            "## Setup\n\n" +
            "Describe any steps that must be done before performing the test.\n\n" +
            "### Scenario / Steps\n\n### Expected outcome\n\n### Tear down\n\n" +
            "Describe what to do after the test\n\n## Test result\n\n" +
            "Protocol of the result of executing this test, latest on top\n"
        )
    with open(path + "/template_automated_test_case.py", "w") as file:
        file.write(
            "import unittest\n\n" +
            "class test_case_name(unittest.TestCase):\n" +
            "    \"\"\"\n    [testcase id=TC0001 story=US0001 requirement=REQ0001]\n\n" +
            "    <Purpose of the test case>\n" +
            "    \"\"\"\n\n\n" +
            "    def test_upper(self):\n" +
            "        self.assertEqual('foo'.upper(), 'FOO')\n\n" +
            "    def test_isupper(self):\n" +
            "        self.assertTrue('FOO'.isupper())\n        self.assertFalse('Foo'.isupper())\n\n" +
            "    def test_split(self):\n"
            "        s = \'hello world\'\n        self.assertEqual(s.split(), ['hello', 'world'])\n" +
            "        # check that s.split fails when the separator is not a string\n" +
            "        with self.assertRaises(TypeError):\n            s.split(2)\n\n" +
            "if __name__ == \'__main__\':\n    unittest.main()\n"
        )


def create_quality_req_template(path):
    with open(path + "/template_quality_req.md", "w") as file:
        file.write(
            "# Document title\n\nDocument description\n\n## Subsection\n\n" +
            "Section description\n\n### [quality id=QR0001 requirement=REQ0005]\n\n" +
            "Quality requirement text\n\n## Another subsection\n\n" +
            "Section description\n\n### [quality id=QR0002 requirement=REQ0007]\n\n" +
            "Quality requirement text\n"
        )


def create_acceptance_test_template(path):
    with open(path + "/template_acceptance_test.feature", "w") as file:
        file.write(
            "# [acceptancetest id=AT0001 story=US0006,US0009]\n" +
            "Feature: <Feature_title>\n\n" +
            "   Scenario: <Scenario_title1>\n" +
            "      When:<Some_action>\n      Then:<Expected_result>\n\n" +
            "   Scenario: <Scenario_title2>\n" +
            "      Given:<Some_precondition>\n      When:<Some_action>\n      Then:<Expected_result>\n"
        )


def generate_templates(path):
    os.makedirs(path, exist_ok=True)
    create_sys_req_template(path)
    create_test_case_template(path)
    create_quality_req_template(path)
    create_acceptance_test_template(path)
