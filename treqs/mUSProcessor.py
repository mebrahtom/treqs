#!/usr/bin/env python3
import getopt
import os
import re
import sys
import datetime
import requests


class USProcessor:

    def __init__(self):
        self.log = open(
            'logs/US_log_'+datetime.datetime.now().strftime("%Y%m%d%H%M%S")+'.md', "w")

    # Sets for all ids
    storySet = set()
    duplicateStorySet = set()

    # NOTE: Not self-contained/effect-free. It uses and mutates the sets in which ids are stored (storySet,duplicateStorySet)
    def processStoryLine(self, line):
        "This processes a single line in a user story file, extracting duplicate IDs and a list of all US ids."

        success = True
        # Extracts the actual user story tag if there is one. Note that this requires the tag to be in a single line.
        m = re.search(r'\[userstory .*?\]', line)
        if m:
            self.log.write('New Story:\n')
            reqtag = m.group(0)
            self.log.write(reqtag+'\n')

            # Extract the id attribute from within the user story tag. Only user stories with id are processed.
            id = re.search(r'(?<=id=).*?(?=[ \]])', reqtag)
            if id:
                id = id.group(0)
                self.log.write(id+'\n')

                # Find duplicate ids. Note that currently, duplicate ids are still processed further.
                if id in self.storySet:
                    self.log.write('Duplicate story id:'+id+'\n')
                    self.duplicateStorySet.add(id)
                    success = False
                else:
                    self.storySet.add(id)
            self.log.write('\n')
        return success

    def processAllUSFiles(self, dir, recursive, filePattern=r'US_.*?\.md'):
        "Processes all files containing user stories, extracting user story tag IDs."

        success = True
        # recursive traversion of root directory
        if recursive:
            for root, directories, filenames in os.walk(dir):
                #	for directory in directories:
                #		self.log.write os.path.join(root, directory)

                for filename in filenames:
                    # Only files matching the given pattern are scanned
                    match = re.search(filePattern, filename)
                    if match:
                        with open(os.path.join(root, filename), "r") as file:
                            for line in file:
                                success = self.processStoryLine(
                                    line) and success
        else:
            listOfFiles = os.listdir(dir)

            for entry in listOfFiles:
                # Only files matching the given pattern are scanned
                match = re.search(filePattern, entry)
                if match:
                    with open(os.path.join(dir, entry), "r") as file:
                        for line in file:
                            success = self.processStoryLine(line) and success

        # Simple self.log.writeouts of all relevant sets.
        self.log.write('All stories:\n')
        for currentStory in self.storySet:
            self.log.write(currentStory+'\n')
        self.log.write('\n')

        self.log.write('Duplicate story IDs:\n')
        for currentID in self.duplicateStorySet:
            self.log.write(currentID+'\n')
        return success

    def processAllUS(self, dir, recursive, filePattern=r'US_.*?\.md'):
        "Shorthand, executing helper methods for user story extraction."
        success = self.processAllUSFiles(
            dir, recursive, filePattern)
        self.log.close()
        return success

    def generate_issue_stories(self, repoPath, artsDir, projectId):
        try:
            response = requests.get("https://gitlab.com/api/v4/projects/"+projectId +
                                    "/issues", headers={"PRIVATE-TOKEN": "VLk2sbgb3Pk6um7BubXB"})
            data = response.json()
            issue_dir = repoPath+"/"+artsDir+"/userstory"
            os.makedirs(issue_dir, exist_ok=True)
            open(
                issue_dir+"/US_stories_from_gitlab_issues.md", "w").close()
            if data['error']=="invalid_token":
                print("Please provide a valid token")
            for currentItems in data:
                if "userstory" in currentItems['labels']:
                    if currentItems['description'] != "":
                        with open(issue_dir+"/US_stories_from_gitlab_issues.md", "a+") as file:
                            file.write("\n[userstory]\n")
                            file.write(currentItems['description'])
        except Exception as e:
            print(e)
