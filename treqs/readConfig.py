import sys
import os
import json
from git import Repo
repo = Repo('.', search_parent_directories=True)
path = repo.working_tree_dir
path = os.path.normpath(path)


def readConfig():
    for root, _, files in os.walk(path):
        for file in files:
            filePath = os.path.join(root, file)
            if file == ".config.json" and os.path.dirname(os.path.dirname(filePath)) == path:
                with open(filePath, "r") as f:
                    data = json.load(f)
                break
    return data
