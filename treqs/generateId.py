import argparse
import os
import re
import sys
import json
import base64
import boto3
import mysql.connector
from git import Repo
from datetime import datetime
from treqs import createDB
from treqs import dataEncDec
from treqs import readConfig
repo = Repo('.', search_parent_directories=True)
path = repo.working_tree_dir
path = os.path.normpath(path)
configFileExists = False
reader = repo.config_reader()
email = reader.get_value("user", "email")
now = datetime.now().date()
date = now.strftime('%Y-%m-%d')


def get_user_input():
    """Receives user input from the command line.
    """
    parser = argparse.ArgumentParser(
        description='ID generator for treqs')
    parser.add_argument('--artifact', '-a', type=str, required=True,
                        help="Type of artifact for which an ID needs to be generated")
    parser.add_argument('--num', '-n', type=int,
                        help="The number of IDs to be generated!")
    args = parser.parse_args()
    if args.num:
        num = args.num
    else:
        num = 1
    if args.artifact == "tc":
        generate_id("tc", num)
    elif args.artifact == "sr":
        generate_id("sr", num)
    elif args.artifact == "qr":
        generate_id("qr", num)
    elif args.artifact == "us":
        generate_id("us", num)
    elif args.artifact == "at":
        generate_id("at", num)
    else:
        print("Command option not known!")
        sys.exit(0)


def generate_id(artifactType, num):
    data = readConfig.readConfig()
    artifacts = data["artifacts"]
    sysReq = artifacts["sysReq"]
    qualityReq = artifacts["qualityReq"]
    testCase = artifacts["testCase"]
    acceptanceTest = artifacts["acceptanceTest"]
    userStory = artifacts["userStory"]
    treqsDB = data["treqsDB"]
    sysReqIdPrefix = sysReq["idPrefix"]
    qualityReqIdPrefix = qualityReq["idPrefix"]
    testCaseIdPrefix = testCase["idPrefix"]
    acceptanceTestIdPrefix = acceptanceTest["idPrefix"]
    userStoryIdPrefix = userStory["idPrefix"]
    hostName = treqsDB["hostName"]
    userName = treqsDB["userName"]
    userPass = treqsDB["userPass"]
    dbName = treqsDB["dbName"]
    portNumber = treqsDB["portNumber"]
    session = boto3.session.Session()
    userName = dataEncDec.decryptData(session, userName)
    userPass = dataEncDec.decryptData(session, userPass)
    conn = None
    try:
        createDB.create_db(hostName, userName, userPass, dbName, portNumber)
        conn = mysql.connector.connect(
            host=hostName, port=portNumber, user=userName, passwd=userPass, database=dbName)
        if conn.is_connected:
            cursor = conn.cursor()
            if artifactType == "sr":
                select_query = """SELECT sysreqid FROM sysreqid_tbl WHERE id = (SELECT MAX(id) FROM sysreqid_tbl) """
                idPrefix = sysReqIdPrefix
            elif artifactType == "qr":
                select_query = """SELECT qualityreqid FROM qualityreqid_tbl WHERE id = (SELECT MAX(id) FROM qualityreqid_tbl) """
                idPrefix = qualityReqIdPrefix
            elif artifactType == "us":
                select_query = """SELECT userstoryid FROM userstoryid_tbl WHERE id = (SELECT MAX(id) FROM userstoryid_tbl) """
                idPrefix = userStoryIdPrefix
            elif artifactType == "tc":
                select_query = """SELECT testid FROM testid_tbl WHERE id = (SELECT MAX(id) FROM testid_tbl) """
                idPrefix = testCaseIdPrefix
            elif artifactType == "at":
                select_query = """SELECT acceptancetestid FROM acceptancetestid_tbl WHERE id = (SELECT MAX(id) FROM acceptancetestid_tbl) """
                idPrefix = acceptanceTestIdPrefix
            cursor.execute(select_query)
            result = cursor.fetchone()
            if result:
                resultId = result[0]
            else:
                resultId = idPrefix+"0000"
            idSuffix = resultId[-4:]
            idSuffix = int(idSuffix)
            print("New available IDs:\n")
            newSuffix = idSuffix+1
            for _ in range(num):
                newId = idPrefix+str("%04d" % (newSuffix))
                newSuffix += 1
                if artifactType == "sr":
                    cursor.execute(
                        """INSERT INTO sysreqid_tbl(sysreqid, user_email, date_generated)VALUES(%s, %s, %s)""", (newId, email, date))
                    conn.commit()
                    print(newId)
                elif artifactType == "qr":
                    cursor.execute(
                        """INSERT INTO qualityreqid_tbl(qualityreqid, user_email, date_generated)VALUES(%s, %s, %s)""", (newId, email, date))
                    conn.commit()
                    print(newId)
                elif artifactType == "tc":
                    cursor.execute(
                        """INSERT INTO testid_tbl(testid, user_email, date_generated)VALUES(%s, %s, %s)""", (newId, email, date))
                    conn.commit()
                    print(newId)
                elif artifactType == "us":
                    cursor.execute(
                        """INSERT INTO userstoryid_tbl(userstoryid, user_email, date_generated)VALUES(%s, %s, %s)""", (newId, email, date))
                    conn.commit()
                    print(newId)
                elif artifactType == "at":
                    cursor.execute(
                        """INSERT INTO acceptancetestid_tbl(acceptancetestid, user_email, date_generated)VALUES(%s, %s, %s)""", (newId, email, date))
                    conn.commit()
                    print(newId)
    except Exception as e:
        print("Database connectivity error!", e)
        sys.exit(0)
    finally:
        if conn is not None:
            cursor.close()
            conn.close()
