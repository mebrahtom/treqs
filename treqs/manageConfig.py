#!/usr/bin/env python3
import json
import sys
import os
import argparse
import git
import base64
import boto3
from treqs import dataEncDec
from treqs import generateTemplates


def manage_config():
    """Create configuration file."""
    if len(sys.argv) == 1:
        repo = git.Repo('.', search_parent_directories=True)
        repoPath = repo.working_tree_dir
        if os.path.isdir(repoPath):
            path = os.path.normpath(repoPath)
            repoManager = input(
                "The URL of your repository management service(https://gitlab.com):")
            if repoManager == "":
                repoManager = "https://gitlab.com"
            groupName = input("GitLab group name:")
            artsDir = input("Parent directory of T-reqs artifacts(treqs):")
            if artsDir == "":
                artsDir = "treqs"
                os.makedirs(path+"/"+artsDir, exist_ok=True)
            else:
                os.makedirs(path + "/" + artsDir, exist_ok=True)
            sysReqPattern = input(
                "File name pattern for system requirements("+r"SR_.*?\.md"+"):")
            if sysReqPattern == "":
                sysReqPattern = r"SR_.*?\.md"
            sysReqIdPrefix = input(
                "ID prefix for system requirements(SR):")
            if sysReqIdPrefix == "":
                sysReqIdPrefix = "SR"
            qualityReqPattern = input(
                "File name pattern for quality requirements("+r"QR_.*?\.md"+"):")
            if qualityReqPattern == "":
                qualityReqPattern = r"QR_.*?\.md"
            qualityReqIdPrefix = input(
                "ID prefix for quality requirements(QR):")
            if qualityReqIdPrefix == "":
                qualityReqIdPrefix = "QR"
            usPattern = input(
                "File name pattern for user stories("+r"US_.*?\.md"+"):")
            if usPattern == "":
                usPattern = r"US_.*?\.md"
            usIdPrefix = input(
                "ID prefix for user stories(US):")
            if usIdPrefix == "":
                usIdPrefix = "US"
            tcPattern = input(
                "File name pattern for test cases("+r"TC_.*?(\.py|\.md)$"+"):")
            if tcPattern == "":
                tcPattern = r"TC_.*?(\.py|\.md)$"
            tcIdPrefix = input(
                "ID prefix for test cases (TC):")
            if tcIdPrefix == "":
                tcIdPrefix = "TC"
            atPattern = input(
                "File name pattern for acceptance tests("+r"AT_.*?\.feature"+"):")
            if atPattern == "":
                atPattern = r"AT_.*?\.feature"
            atIdPrefix = input(
                "ID prefix for acceptance tests(AT):")
            if atIdPrefix == "":
                atIdPrefix = "AT"
            hostName = input("The endpoint of your database instance on AWS:")
            dbName = input("Database name:")
            if dbName == "":
                print("Database cannot be empty!")
                sys.exit(0)
            userName = input("Database username:")
            if userName == "":
                print("Database username cannot be empty!")
                sys.exit(0)
            userPass = input("Database password:")
            if userPass == "":
                print("Database password cannot be empty!")
                sys.exit(0)
            portNumber = input("Database port number:")
            if portNumber == "":
                portNumber = 3306
            else:
                portNumber = int(portNumber)
            aliasName = input(
                "Alias name for your AWS KMS customer master key(CMK):")
            if aliasName == "":
                print("Alias name for your AWS CMK cannot be empty!")
                sys.exit(0)
            session = boto3.session.Session()
            userName = dataEncDec.encryptData(
                session, userName, "alias/" + aliasName)
            userPass = dataEncDec.encryptData(
                session, userPass, "alias/"+aliasName)
            if os.path.exists(path + "/" + artsDir):
                data = {
                    "repoManager": repoManager,
                    "groupName": groupName,
                    "artsDir": artsDir,
                    "artifacts": {
                        "sysReq": {
                            "fileNamePattern": sysReqPattern,
                            "idPrefix": sysReqIdPrefix
                        },
                        "qualityReq": {
                            "fileNamePattern": qualityReqPattern,
                            "idPrefix": qualityReqIdPrefix
                        },
                        "testCase": {
                            "fileNamePattern": tcPattern,
                            "idPrefix": tcIdPrefix
                        },
                        "acceptanceTest": {
                            "fileNamePattern": atPattern,
                            "idPrefix": atIdPrefix
                        },
                        "userStory": {
                            "fileNamePattern": usPattern,
                            "idPrefix": usIdPrefix
                        }
                    },
                    "treqsDB": {
                        "hostName": hostName,
                        "dbName": dbName,
                        "userName": userName,
                        "userPass": userPass,
                        "portNumber": portNumber,
                        "aliasName": aliasName
                    }
                }
                # Write configuration data to the config.json file
                with open(path+"/"+artsDir+"/.config.json", "w") as file:
                    json.dump(data, file, indent=2)
                templatePath = path + "/"+artsDir + "/templates"
                generateTemplates.generate_templates(templatePath)
            print("-----------------------------------------------")
            print("You have set up your configuration successfully!")
            print("Configuration file path: " + path +
                  "/" + artsDir + "/.config.json")
            print("Template files path: "+templatePath)

        else:
            print("Such repository doesn't exist in your system!")
    else:
        print("Wrong arguments!, please enter treqsconfig to continue.")
