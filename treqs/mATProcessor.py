#!/usr/bin/env python3
import getopt
import os
import re
import sys
import datetime


class ATProcessor:

    def __init__(self):
        self.log = open(
            'logs/AT_log_'+datetime.datetime.now().strftime("%Y%m%d%H%M%S")+'.md', "w")

    # Sets for all ids
    storySet = set()
    noUSTracingSet = set()
    testIDSet = set()
    duplicateIDSet = set()

    # NOTE: Not self-contained/effect-free. It uses and mutates the sets in which ids are stored (storySet,noUSTracingSet,reqSet,noReqTracingSet,testIDSet,duplicateIDSet)
    def processATLine(self, line):
        "This processes a single line in a Gherkin feature file, extracting duplicate IDs, missing traces, and a list of all traces to US."

        success = True
        # Extracts the actual test case tag if there is one. Note that this requires the tag to be in a single line.
        m = re.search(r'\[acceptancetest .*?\]', line)
        if m:
            self.log.write('New Acceptance Test:\n')
            reqtag = m.group(0)
            self.log.write(reqtag+'\n')

            # Extract the id attribute from within the test case tag. Only test cases with id are processed.
            id = re.search(r'(?<=id=).*?(?=[ \]])', reqtag)
            if id:
                id = id.group(0)
                self.log.write(id+'\n')

                # Find duplicate ids. Note that currently, duplicate ids are still processed further.
                if id in self.testIDSet:
                    self.log.write('Duplicate AT id:'+id+'\n')
                    self.duplicateIDSet.add(id)
                    success = False
                else:
                    self.testIDSet.add(id)

                # Find all story attributes. Supports also multiple story attributes in theory.
                stories = re.findall(r'(?<=story=).*?(?=[ \]])', reqtag)

                # Find test cases without user story traces
                if len(stories) == 0:
                    self.log.write(
                        'Warning: Acceptance Test is not traced to a user story!\n')
                    self.noUSTracingSet.add(id)
                    success = False
                else:
                    for currentUS in stories:
                        # Support potential commas in an issue attribute
                        splitstories = re.split(',', currentUS)
                        for currentStory in splitstories:
                            self.log.write(currentStory+'\n')
                            self.storySet.add(currentStory)

            self.log.write('\n')
        return success

    def processAllAT(self, dir, recursive, filePattern=r'AT_.*?(.feature)'):

        success = True

        # recursive traversion of root directory
        if recursive:
            for root, directories, filenames in os.walk(dir):
                #	for directory in directories:
                #		self.log.write os.path.join(root, directory)
                for filename in filenames:
                    # Only files matching the given pattern are scanned
                    match = re.search(filePattern, filename)
                    if match:
                        with open(os.path.join(root, filename), "r") as file:
                            for line in file:
                                success = self.processATLine(
                                    line) and success
        else:
            listOfFiles = os.listdir(dir)
            # Only files matching the given pattern are scanned
            for f in listOfFiles:
                match = re.search(filePattern, f)
                if match:
                    with open(os.path.join(dir, f), "r") as file:
                        for line in file:
                            success = self.processATLine(
                                line) and success

        # Simple self.log.writeouts of all relevant sets.
        self.log.write('All story traces:\n')
        for currentStory in self.storySet:
            self.log.write(currentStory+'\n')
        self.log.write('\n')

        self.log.write('Acceptance Tests without traces to stories:\n')
        for currentID in self.noUSTracingSet:
            self.log.write(currentID+'\n')
        self.log.write('\n')

        self.log.write('Duplicate Acceptance Test IDs:\n')
        for currentID in self.duplicateIDSet:
            self.log.write(currentID+'\n')

        self.log.close()
        return success
